package bot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var enterPhoneNumberMenuKeyboard = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButtonContact("📱 Telefon raqamni jo'natish"),
		tgbotapi.NewKeyboardButton("⬅️ Orqaga"),
	),
)