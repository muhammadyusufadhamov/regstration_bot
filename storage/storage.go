package storage

import (
	"database/sql"
	"errors"
	"fmt"
	"time"
)

const (
	EnterFullnameStep    string = "enter_fullname"
	EnterPhoneNumberStep string = "enter_phone_number"
	RegisteredStep       string = "registered"
)

type User struct {
	TgId        int64
	TgName      string
	Fullname    *string
	PhoneNumber *string
	Step        string
	CreatedAt   *time.Time
}

type StorageI interface {
	Create(u *User) (*User, error)
	Get(id int64) (*User, error)
	GetOrCreate(tgID int64, tgName string) (*User, error)
	ChangeField(tgId int64, field, value string) error
	ChangeStep(tgID int64, step string) error
}

type storagePg struct {
	db *sql.DB
}

func NewStoragePg(db *sql.DB) StorageI {
	return &storagePg{
		db: db,
	}
}

func (s *storagePg) Create(user *User) (*User, error) {
	query := `
		insert into users (
			tg_id,
			tg_name,
			step
		) values ($1, $2, $3)
		returning
			fullname,
			phone_number,
			created_at
	`
	err := s.db.QueryRow(
		query,
		user.TgId,
		user.TgName,
		user.Step,
	).Scan(
		&user.Fullname,
		&user.PhoneNumber,
		&user.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *storagePg) Get(id int64) (*User, error) {
	var result User

	query := `
		select
			tg_id,
			fullname,
			phone_number,
			step,
			created_at
		from users where tg_id=$1
	`

	row := s.db.QueryRow(query, id)
	err := row.Scan(
		&result.TgId,
		&result.Fullname,
		&result.PhoneNumber,
		&result.Step,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (s *storagePg) GetOrCreate(tgId int64, tgName string) (*User, error) {
	user, err := s.Get(tgId)
	if errors.Is(err, sql.ErrNoRows) {
		u, err := s.Create(&User{
			TgId:   tgId,
			TgName: tgName,
			Step:   EnterFullnameStep,
		})
		if err != nil {
			return nil, err
		}
		user = u
	} else if err != nil {
		return nil, err
	}

	return user, nil
}

func (s *storagePg) ChangeField(tgId int64, field, value string) error {
	query := fmt.Sprintf("update users set %s=$1 where tg_id=$2", field)

	result, err := s.db.Exec(query, value, tgId)
	if err != nil {
		return err
	}
	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (s *storagePg) ChangeStep(tgid int64, step string) error {
	query := `update users set step=$1 where tg_id=$2`

	result, err := s.db.Exec(query, step, tgid)
	if err != nil {
		return err
	}

	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}
