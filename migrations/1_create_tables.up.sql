create table if not exists users (
    tg_id int primary key,
    tg_name varchar(50) not null,
    fullname varchar(100),
    phone_number varchar(50) unique,
    step varchar(50) not null,
    created_at timestamp with time zone default current_timestamp 
);